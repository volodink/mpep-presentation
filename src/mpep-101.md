---
marp: true
theme: lection
paginate: true
---

<!-- _class: lead -->
<!-- _paginate: false -->

# НИРС на кафедре - залог Вашего образования

# Текущие задачи и перспективы

#

#

#

#

#

#

#

##### Кирилл Пискаев
##### Константин Володин

---

# Немного о предметной области

Специальность: __1__ (Информационные системы и технологии)

Направление: __3__ (ИС, ИД, ИБ)

ОПОП: __63__
Количество рабочих программ: __56__

Итого: __189__ документов

<br>
<br>

#### Необходимо внести изменения за 1 день

---

<!-- _paginate: false -->

# Цели проекта

Разработка программного обеспечения системы ведения электроного документооборота 
кафедры 

## Состав ПО 

1. Модуль подготовки рабочих программ дисциплин
2. Модуль подготовки матрицы компетенций
3. Модуль кадрового обеспечения
4. Модуль работы с литературой
5. Модуль импорта и синхронизации
6. Модуль работы с подписанными документами 

![bg right:30%](img/docs.jpg)

---
## Требования к ПО

1. Портабельность

2. Совместимость с ОС: __Windows__, __Linux__

3. Интерфейс пользователя: окна рабочего стола (Desktop)

4. Дизайн интерфейса должен следовать требованиям к __UX-дизайну__

5. Импорт данных из форматов __Excel__ (xls, xlsx), __Word__ (doc, docx) 

6. Выгрузка документов в форматах __DOCX__ и __PDF__

7. Защищенность от __НСД__

![bg right:30%](img/docs.jpg)

---
<!-- _class: split -->
<!-- _paginate: false -->

# Технологии проекта

<div class=ldiv>

# Python-разработка (GeekBrains)

![h:64px](https://for-landing.hb.bizmrg.com/images/python.svg) ![h:64px](https://for-landing.hb.bizmrg.com/images/sql.svg) ![h:64px](https://for-landing.hb.bizmrg.com/images/nosql.svg)
![h:64px](https://for-landing.hb.bizmrg.com/images/django_framework.svg) ![h:64px](https://for-landing.hb.bizmrg.com/images/javascript.svg) ![h:64px](https://for-landing.hb.bizmrg.com/images/css.svg)
![h:64px](https://for-landing.hb.bizmrg.com/images/gitsvg.svg) ![h:64px](https://for-landing.hb.bizmrg.com/images/docker.png)

Средняя зарплата
Python-разработчика: __70 000 ₽__

Цена курса: __66 000+ ₽__

</div>
<div class=rdiv>

# Проект кафедры ИТС (ПензГТУ)

![h:64px](https://for-landing.hb.bizmrg.com/images/python.svg) ![h:64px](https://for-landing.hb.bizmrg.com/images/sql.svg) ![h:64px](https://for-landing.hb.bizmrg.com/images/nosql.svg)
![h:64px](https://upload.wikimedia.org/wikipedia/commons/thumb/e/e6/Python_and_Qt.svg/1200px-Python_and_Qt.svg.png) ![h:64px](https://for-landing.hb.bizmrg.com/images/javascript.svg) ![h:64px](https://for-landing.hb.bizmrg.com/images/css.svg)
![h:64px](https://for-landing.hb.bizmrg.com/images/gitsvg.svg) ![h:64px](https://for-landing.hb.bizmrg.com/images/docker.png)
![h:64px](https://formulahendry.gallerycdn.vsassets.io/extensions/formulahendry/terminal/0.0.10/1500738160902/Microsoft.VisualStudio.Services.Icons.Default) ![h:64px](https://blog.bissquit.com/wp-content/uploads/2019/01/powershell-logo-new.png)

</div>

---

<!-- _class: invert2_10 -->
<!-- _paginate: false -->
<!-- _backgroundColor: yellow -->

# <!-- fit -->JS || !JS

Есть кто ботает JS ?

---

<style scoped>
section ol {
  margin-left: 1.2cm;
  margin-top: 0;
  margin-bottom: 0;
}
</style>

# Кого мы ищем

:snake: Разработчик __Python__

1) Разработка приложения на __Python core__, __PyQT__, __Motor__, __PyMongo__, __FastAPI__
2) Разработка схемы данных и запросов для __MongoDB__

:robot: Тестировщик (__Python__)
1) _Unit_-тесты (__pytest__)
2) Фукциональные тесты (__pytest-qt__)
3) Тесты на производительность 

:page_facing_up: Аналитик, технический писатель
1) Ведение документации на код и схему БД

---

<!-- _paginate: false -->

# Преимущества участия

1. Применение теоретических знаний на практике

2) Навыки командной работы по Agile

3) Проектное обучение по основным дисциплинам

4) Применения стека технологий в рамках одного проекта

5) Проект в портфолио

6) Отбор в научный актив кафедры 

![bg right:50%](img/prog.jpg)

---

<!-- _paginate: false -->
<!-- _class: oneimage -->

![bg](https://sdtimes.com/wp-content/uploads/2017/06/DevOps-Marketplace-Infinity-Loop.png)

---

<!-- _class: diagram_minimal -->
<!-- _paginate: false -->

# Регистрация на цикл семинаров (bootcamp)
![bg right](img/qrcode.png)
https://docs.google.com/forms/d/e/1FAIpQLSfoPWMXn39ol76oZshPU4DMWkgUahr1KYZ3pt3td7u_AIuhLw/viewform

![h:360px](img/rick.png)
