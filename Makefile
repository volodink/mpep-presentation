.PHONY: all clean live 

all: clean
	java -jar .devcontainer/lib/plantuml.jar src/diagrams/*.puml
	marp --html src/mpep-101.md -o build/mpep-definitions.pdf

live:
	marp --html --server ./src/

clean:
	rm -rf build
	rm -rf builds
